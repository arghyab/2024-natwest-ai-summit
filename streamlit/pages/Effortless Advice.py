import pandas as pd
import numpy as np
from sklearn.calibration import LabelEncoder
from sklearn.compose import ColumnTransformer
import streamlit as st
import numpy as np
import joblib

from dotenv import load_dotenv
load_dotenv()

from sklearn.model_selection import train_test_split
from sklearn.preprocessing import OneHotEncoder, StandardScaler
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, classification_report

# Load data
df = pd.read_csv('data/data.csv')
st.sidebar.header("Effortless Advice")

if 'messages' not in st.session_state or st.button("Reset chat"):
    st.session_state.messages = []
    st.session_state.input_text = ""

feature_column_names=['Maturity_Date', 'Bank_Customer_Classification', 'Turnover', 'Asset_Condition', 'Asset_Type', 'Asset_Purpose']
target_column_name='Product_Name'

def train_model():
    label_encoder = LabelEncoder()
    ct = ColumnTransformer(transformers=[('encoder', OneHotEncoder(handle_unknown='ignore'), feature_column_names)], remainder='passthrough')
    # Standardize features
    scaler = StandardScaler(with_mean=False)
    # Feature engineering
    features = df[feature_column_names]
    target = df[target_column_name]  # Example target: customers likely to need large asset finance loans
    data_encoded = ct.fit_transform(features)
    # Split data
    X_train, X_test, y_train, y_test = train_test_split(data_encoded, label_encoder.fit_transform(target), train_size=0.9, random_state=42)

    # y_train_encoded = label_encoder.fit_transform(y_train)
    # y_test_encoded = label_encoder.fit_transform(y_test)
    X_train_scaled = scaler.fit_transform(X_train)
    # X_test_scaled = scaler.transform(X_test)

    # Train model
    model = RandomForestClassifier(random_state=42)
    model.fit(X_train_scaled, y_train)
    joblib.dump(model, 'model/random_forest_model.pkl')
    joblib.dump(scaler, 'model/scaler.pkl')
    joblib.dump(ct, 'model/columnTransformer.pkl')
    joblib.dump(label_encoder, 'model/labelEncoder.pkl')

    # Evaluate model
    # y_pred = model.predict(X_test_scaled)
    # st.write("Accuracy:", accuracy_score(y_test, y_pred))
    # st.write("Classification Report:\n", classification_report(y_test, y_pred))

if st.button("Train model"):
    train_model()
def submit():
    st.session_state.input_text = st.session_state.input_widget
    st.session_state.input_widget = ""

st.text_input("Party Number: ", key="input_widget", on_change=submit)

page = st.query_params.get("page", ["home"])[0]

existing_data = pd.read_csv("data/existingData.csv")

def str_to_numeric(value):
    try:
        # Remove commas and convert to float
        return float(value.replace(',', ''))
    except ValueError:
        return None  # Return None for non-convertible values
    
def get_predictions_response():
    model=joblib.load('model/random_forest_model.pkl')
    scaler=joblib.load('model/scaler.pkl')
    ct=joblib.load('model/columnTransformer.pkl')
    label_encoder=joblib.load('model/labelEncoder.pkl')
    existing_data_df = existing_data[existing_data['Party Number'].isin([st.session_state.input_text])]
    if(not existing_data_df.empty):
        filtered_existing_data_df = existing_data_df
        query = filtered_existing_data_df[feature_column_names]
        filtered_existing_data_df['Predicted_Product_Name']=label_encoder.inverse_transform(model.predict(scaler.transform(ct.transform(query))))
        final_data_frame=filtered_existing_data_df[['Contract_Number','Product_Name','Predicted_Product_Name','Capital_locked']]
        final_data_frame.loc[filtered_existing_data_df['Product_Name'] == filtered_existing_data_df['Predicted_Product_Name'], 'Capital_locked'] = '-'
        final_data_frame=final_data_frame.rename(columns={'Capital_locked': 'Unlocked Capital','Predicted_Product_Name':'Best Fit Proposal', 'Product_Name':'Product In Use'})
        final_data_frame
        numeric_values = pd.to_numeric(final_data_frame['Unlocked Capital'].apply(str_to_numeric), errors='coerce')
        sum_numeric = numeric_values[~numeric_values.isna()].sum()
        st.write(f"{st.session_state.input_text} {sum_numeric}")
    else: "No party details found"

# Sending Messages
if st.session_state.input_text != "":
    get_predictions_response()


st.markdown(
    """
    ### Footer

    Copyright © 2024. Your Company. All rights reserved.
    """
)
